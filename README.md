# test-yourart




## Getting started

This project was created using known librairies/frameworks. The front was created from a new vite.js project `yarn create vite ... front`.
These application is dockerized and the code is running inside container.

## Start the application - quick mode

- open a new terminal at the root of the project. Run these following commands

```
docker-compose build && docker-compose up
```
- open a new tab in the web client and enter this URL [http://client.docker.localhost/](http://client.docker.localhost/)

## Develop the application

- In order to develop or run custom command inside the application, you can do through docker container. Follow these steps:

- Replace in the docker-compose.yml (front service)
```
# entrypoint: yarn dev
entrypoint: /bin/bash -c 'while sleep 1000; do :; done;'
```

- Launch container service after update
```
docker-compose down
docker-compose up
```

- access container and develop inside it (using VScode). You will need vscode extension. Follow this [devcontainer documentation](https://code.visualstudio.com/docs/devcontainers/containers) if needed

- the project inside container are stored in the /usr/src folder
