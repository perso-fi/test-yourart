import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import * as fakeApi from "../../api";
import { Artwork } from "../../models";
import styles from './Artwork.module.css'
import { Breadcrumbs, ProductCard } from "../../components";

export const ArtworkPage = () => {
  let { id } = useParams();
  const [data, setData] = useState<Artwork>();

  useEffect(() => {
    if (id) {
      fakeApi.getMockData(id).then(value => setData(value))
    }
  }, [id])

  const getBreadcumbsValues = () => {
    const values = ['Home'];
    if (data?.category) values.push(data?.category.toLowerCase())
    if (data?.artistShort.fullname) values.push(`${data?.artistShort.fullname} Artworks`.toLowerCase())
    if (data?.fullname) values.push(`${data?.fullname}`.toLowerCase())
    return values;
  }

  return (
    <main className={styles.container}>
      <Breadcrumbs values={getBreadcumbsValues()}/>
      {data && <ProductCard data={data}/>}
    </main>
  )
}