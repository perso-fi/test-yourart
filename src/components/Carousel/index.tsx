import { useEffect, useRef, useState } from "react";
import './index.css'

const IMG_WIDTH = 100;
const DEFAUT_ITEM_MARGIN = 10;
const ITEM_WIDTH = 120;

interface Props {
  images: string[] | undefined
}

export const Carousel = ({ images = [] }: Props) => {
  const totalItems = images.length;
  const containerRef = useRef<HTMLDivElement>(null);
  const [scrollState, setScrollState] = useState<'start'|'end'|null>('start')

  useEffect(() => {
    if (containerRef.current)
      containerRef.current.addEventListener("scroll", handleScrollState);
    
      return () => containerRef.current!.removeEventListener("scroll", handleScrollState);
  }, [containerRef.current]);

  const scrollNext = () => {
    if (containerRef.current) {
      containerRef.current.scrollLeft += ITEM_WIDTH;
    }
  };
  
  const scrollPrevious = () => {
    if (containerRef.current) {
      containerRef.current.scrollLeft -= ITEM_WIDTH;
    }
  };

  const handleScrollState = () => {
    if (containerRef.current) {
      if ((containerRef.current.scrollLeft + containerRef.current.offsetWidth) >= containerRef.current.scrollWidth)
        setScrollState('end')
      else if (containerRef.current.scrollLeft === 0)
        setScrollState('start')
      else
        setScrollState(null)
    }
  }

  return (
    <div  className="carousel-wrapper">
      
      <div className="carousel-btn-wrapper">
        {scrollState !== 'start' && <button onClick={scrollPrevious}>{'<'}</button>}
      </div>
      
      <div ref={containerRef} className="carousel-container">
        <div className="carousel-slider" style={{ width: `${totalItems * ITEM_WIDTH}px`}}>

          {images.map((item, index) => (
              <img 
                key={index} 
                src={item} 
                alt={`img-${index}`} 
                style={{ height: `${IMG_WIDTH}px`, width: `${IMG_WIDTH}px`, margin: `${DEFAUT_ITEM_MARGIN}px`}}
              />
          ))}
         
        </div>
       </div>

       <div className="carousel-btn-wrapper">
        {scrollState !== 'end' && <button onClick={scrollNext}>{'>'}</button>}
      </div>

    </div>
  );
};