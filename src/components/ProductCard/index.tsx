import styles from './style.module.css'
import { Artwork } from '../../models';
import hourglassSvg from '../../assets/hourglass.svg'
import deliveryTruckSvg from '../../assets/deliveryTruck.svg'
import locationSvg from '../../assets/location-pin.svg'
import eyeSvg from '../../assets/eye.svg'
import cubeSvg from '../../assets/cube.svg'
import { Carousel } from '../Carousel';
import { useState } from 'react';

interface Props {
  data: Artwork
}

export const ProductCard = ({ data }: Props) => {

    const [openDescription, setOpenDescription] = useState(false)
    const [openSubject, setOpenSubject] = useState(false)

    return (
      <div className={styles.content}>
        <div className={styles.imgInfoContainer}>

          <div className={styles.imgContainer}>
            <img src={data?.imageUrl} alt="Product image" style={{ width: '100%' }} />
            <div className={styles.imgBtnContainer}>
            <button className={styles.eyeBtn} ><img src={eyeSvg} style={{ width: "12px", marginRight: '10px' }}></img>VIEW IN A ROOM</button>
            <button className={styles.cubeBtn} ><img src={cubeSvg} style={{ width: "12px", marginRight: '10px' }}></img>AR VIEW</button>
            </div>
          </div>

          <div className={styles.infoContainer}>
            
            <div className={styles.nameContainer}>
              <div className={styles.flexcolumn}>
                <div className={styles.fullname}>{data?.fullname}</div>
                <div className={styles.artistFullnameContainer}>
                  <a href={`/profile/${data?.artistId}`} target="_self" style={{ marginRight: '10px', textDecoration: 'none' }}><span className={styles.artistFullname}>{data?.artistShort?.fullname}</span></a>
                  <div style={{ color: '#5d5d5d' }}>{data?.artistShort?.country}</div>
                </div>
                <div className={styles.extraDetailsContainer}>
                  <div style={{ fontSize: 16 }}>{`${data?.category}, ${data?.creationYear}`}</div>
                  <div style={{ fontSize: 16 }}>{`${data?.dimensions.height} H x ${data?.dimensions.width} W x ${data?.dimensions.depth} D in`}</div>
                  <div className={styles.priceText}>{`${data?.price} €`}</div>
                </div>
              </div>
              <div className={styles.style_02} >
                <span title="Like the artwork">☆</span>
              </div>
            </div>
            
            <button className={styles.acquireBtn} type="button">Acquire</button>
            <button className={styles.offerBtn} type="button">Make an offer</button>

            <div><button className={styles.prereserveBtn} ><img src={hourglassSvg} style={{ width: "12px", marginRight: '10px' }}></img>PRE-RESERVE FOR 24 HOURS</button></div>
            <div className={styles.delivryDescription}>In order to obtain an accurate delivery fee, please enter your country of residence and zip code:</div>
            <div className={styles.inputContainer}>
              <select style={{ width: 120 }}>
                <option value="0">Country</option>
                <option value="1">France</option>
                <option value="2">Espagne</option>
              </select>
              <input type="text" placeholder="Zip code" style={{ width: 120, marginLeft: 10 }} pattern="[0-9]+"/>
            </div>
            <div className={styles.delivryText}><img src={deliveryTruckSvg} style={{ width: "12px", marginRight: '10px' }} />Check delivery fee</div>
            <div className={styles.locationText}><img src={locationSvg} style={{ width: "12px", marginRight: '10px' }} />Free pickup in</div>
            <div className={styles.tryForFreeText}><span style={{ marginRight: '10px' }}>✓</span>Try 14 days at home for free</div>

          </div>
        </div>
        
        <div className={styles.details}>
          <div onClick={() => setOpenDescription(!openDescription)} className={styles.descriptionContainer}>
            <h2>DESCRIPTION</h2>
            {openDescription && <div className={styles.descriptiontext}>{data.description}</div>}
          </div>

          <div onClick={() => setOpenSubject(!openSubject)} className={styles.descriptionContainer}>
            <h2>SUBJECT, MEDIUM, STYLES, MATERIAL</h2>
            {openSubject && (
              <div className={styles.subjectContainer}>
                <div><span className={styles.subjectKey}>SUBJECT</span>{data.subjects}</div>
                <div><span className={styles.subjectKey}>MEDIUM</span>{data.mediums}</div>
                <div><span className={styles.subjectKey}>STYLES</span>{data.styles}</div>
                <div><span className={styles.subjectKey}>MATERIAL</span>{data.materials}</div>
              </div>
            )}
          </div>
        </div>

        <div className={styles.carousel}>
          <Carousel images={data?.otherArtworkImages}></Carousel>
        </div>
      </div>
    );
};