import styles from './style.module.css'
import arrowSvg from '../../assets/arrow-right.svg'

interface Props {
  values: string[] | undefined
}

export const Breadcrumbs = ({ values = ['Home'] }: Props) => {

  return (
    <div className={styles.container}>
      {values.map((str, index) => (index === 0) ? 
        <span key={index} className={styles.span}>{str}</span> : 
        <span key={index} className={(index < values.length - 1) ? styles.span : styles.spanlatest}><img src={arrowSvg} className={styles.img}/>{str}</span>
      )}
    </div>
  );
};