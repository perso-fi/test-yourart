import { Routes, Route, Navigate } from 'react-router-dom'
import { ArtworkPage } from './pages/Artwork'

function App() {

  return (
    <Routes>
      <Route path='artwork/:id' element={<ArtworkPage />}/>
      <Route path="*" element={<Navigate to="/artwork/0" replace />} />
    </Routes>
  )
}

export default App
