import { Artwork } from "../models"

export const getMockData = async (id: string): Promise<Artwork> => {
  const response = await fetch(`/example_${id}.json`)
  const data = await response.json()
  return data;
}

